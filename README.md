# List Detail Edit

A simple React Native demo app for demoing the age-old List-Detail-Edit flow.

Made as code sample for job interviews.

![demo](demo.gif)