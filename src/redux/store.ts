import { NavReducerState } from '@/redux/reducers/AppNavReducer'
import {
    CategoryListReducer as categoryListReducer,
    CategoryReducerState,
    CategorySelectReducer as categorySelectReducer
} from './reducers/screens/CategoryReducers'
import { createStore, combineReducers, applyMiddleware } from "redux"
import thunk from "redux-thunk"
import reduxPromises from "redux-promise-middleware"
import transactionReducers, { TransactionReducerState } from './reducers/screens/TransactionReducers'
import transactionDetailReducers, { TransactionDetailReducerState } from './reducers/screens/TransactionDetailReducers'

export interface AppReducersState {
    transactionReducers: TransactionReducerState,
    transactionDetailReducers: TransactionDetailReducerState,
    categoryListReducer: CategoryReducerState,
    categorySelectReducer: CategoryReducerState,
}

export default createStore(
    combineReducers({
        transactionReducers,
        transactionDetailReducers,
        categoryListReducer,
        categorySelectReducer,
    }
    ),
    {},
    applyMiddleware(thunk, reduxPromises)
)
