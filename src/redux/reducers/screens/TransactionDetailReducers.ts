import { Transaction, UserCategory } from '@/data'
import {
    TransactionDetailAction,
    TransactionDetailActionType
} from '@/redux/actions/screens/Details/TransactionDetailActionType'


const initialState: TransactionDetailReducerState = {
    isLoading: false,
    dialogVisible: false
}

export type TransactionDetailReducerState = {
    isLoading: boolean
    transaction?: Transaction
    updatedCategory?: UserCategory
    isUpdating?: boolean
    dialogVisible: boolean
}

export default (state = initialState, action: TransactionDetailAction): TransactionDetailReducerState => {
    switch (action.type) {
        case TransactionDetailActionType.GET_TRANSACTION_DETAILS_START:
            return {
                ...state,
                isLoading: true,
            }
        case TransactionDetailActionType.GET_TRANSACTION_DETAILS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                transaction: action.payload?.transaction,
            }
        case TransactionDetailActionType.GET_TRANSACTION_DETAILS_FAIL:
            return {
                ...state,
                isLoading: false,
                transaction: undefined
            }
        case TransactionDetailActionType.INVALIDATE:
            return initialState
        case TransactionDetailActionType.UPDATE_TRANSACTION_USER_CATEGORY_START:
            return {
                ...state,
                isUpdating: true,
            }
        case TransactionDetailActionType.UPDATE_TRANSACTION_USER_CATEGORY_SUCCESS:
            if (state.transaction?.id === action.payload?.updatedTransactionId)
                return {
                    ...state,
                    updatedCategory: action.payload?.updatedCategory,
                    isUpdating: false
                }
            else return state
        case TransactionDetailActionType.UPDATE_TRANSACTION_USER_CATEGORY_FAIL:
            return {
                ...state,
                isUpdating: false
            }
        case TransactionDetailActionType.SHOW_DIALOG:
            return {
                ...state,
                dialogVisible: true
            }
        case TransactionDetailActionType.HIDE_DIALOG:
            return {
                ...state,
                dialogVisible: false
            }
        default:
            return state
    }
}
