import {
    CategoryListAction,
    CategorySelectAction,
    CategorySelectActionType
} from '@/redux/actions/screens/Category/GategoryActionTypes'
import { UserCategory } from '@/data'
import { CategoryListActionType } from "@/redux/actions/screens/Category/GategoryActionTypes"

const initialState: CategoryReducerState = {
}

export type CategoryReducerState = {
    isLoading?: boolean
    categories?: UserCategory[]
    newSelectedId?: string
}

export const CategoryListReducer = (state = initialState, action: CategoryListAction): CategoryReducerState => {
    switch (action.type) {
        case CategoryListActionType.GET_USER_CATEGORY_LIST_START:
            return {
                ...state,
                isLoading: true
            }
        case CategoryListActionType.GET_USER_CATEGORY_LIST_SUCCESS:
            return {
                ...state,
                isLoading: false,
                categories: action.payload
            }
        case CategoryListActionType.GET_USER_CATEGORY_LIST_FAIL:
            return {
                isLoading: false,
                categories: undefined
            }
        default:
            return state
    }
}

export const CategorySelectReducer = (state = initialState, action: CategorySelectAction): CategoryReducerState => {
    switch (action.type) {
        case CategorySelectActionType.SELECT_CATEGORY:
            return {
                ...state,
                newSelectedId: action.payload
            }
        case CategorySelectActionType.RESET:
            return {
                ...state,
                newSelectedId: undefined
            }
        default:
            return state
    }
}
