import { Transaction } from '@/data'
import { TransactionAction, TransactionActionType } from '@/redux/actions/screens/Transactions/TransactionActionType'

const initialState: TransactionReducerState = {
    isLoading: false
}

export type TransactionReducerState = {
    isLoading: boolean
    transactions?: Transaction[]
}

export default (state = initialState, action: TransactionAction): TransactionReducerState => {
    switch (action.type) {
        case TransactionActionType.GET_TRANSACTIONS_START:
            return {
                isLoading: true
            }
        case TransactionActionType.GET_TRANSACTIONS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                transactions: action.payload ?? []
            }
        case TransactionActionType.GET_TRANSACTIONS_FAIL:
            return {
                isLoading: false,
                transactions: undefined
            }
        default:
            return state
    }
}
