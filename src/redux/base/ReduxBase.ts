import { AppReducersState } from '../store'
import { Action } from "redux"

export interface StateConsumer {
    (): AppReducersState
}

/**
 * @param T: ActionType (Provided through 'enum ActionTypeName')
 * @param A: Type/Class that extends Action<T>
 */
export interface Dispatch<T, A> { 
    <T extends A>(action: T): T 
    (f: (d: Dispatch<T, A>, s: StateConsumer) => void): void
}

/**
 * @param T: ActionType (Provided through 'enum ActionTypeName')
 * @param P: Action Payload type
 * @param A: Class that extends Action<T> 
 */
export class ActionBase<T, P, A extends Action<T>> {
    dispatch?: Dispatch<T, A>
    getState?: StateConsumer
    type: T
    payload?: P
    error?: Error

    constructor(
        dispatch: Dispatch<T, A>,
        getState: StateConsumer,
        type: T,
        payload?: P,
        error?: Error,
    ) {
        this.dispatch = dispatch
        this.getState = getState
        this.type = type
        this.payload = payload
        this.error = error
    }
}
