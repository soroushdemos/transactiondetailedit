import { TransactionActionType, TransactionDispatch } from './TransactionActionType'
import Api from '@/api/ApiClient'

export const fetchTransactions = (retryCount?: number) => (dispatch: TransactionDispatch) => {
    dispatch({
        type: TransactionActionType.GET_TRANSACTIONS_START
    })
    Api.client.fetchTransactions()
        .then((response) => {
            dispatch({
                type: TransactionActionType.GET_TRANSACTIONS_SUCCESS,
                payload: response
            })
        })
        .catch((error) => {
            if (!retryCount || retryCount < Api.MAX_RETRIES)
                dispatch(fetchTransactions((retryCount ?? 0) + 1))
            else
                dispatch({
                    type: TransactionActionType.GET_TRANSACTIONS_FAIL
                })
            return false
        })
}