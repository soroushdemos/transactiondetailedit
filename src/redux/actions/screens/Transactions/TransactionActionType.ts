import { Transaction } from "@/data"
import { Dispatch, ActionBase } from "@/redux/base/ReduxBase"
import { Action } from "redux"

export enum TransactionActionType {
    GET_TRANSACTIONS_START = "GET_TRANSACTIONS_START_ACTION",
    GET_TRANSACTIONS_SUCCESS = "GET_TRANSACTIONS_SUCCESS_ACTION",
    GET_TRANSACTIONS_FAIL = "GET_TRANSACTIONS_FAIL_ACTION"
}

export type TransactionAction = ActionBase<TransactionActionType, Transaction[], Action<TransactionActionType>>
export interface TransactionDispatch extends Dispatch<TransactionActionType, TransactionAction> {}