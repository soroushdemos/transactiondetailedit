import Toast from 'react-native-tiny-toast'
import {
    TransactionDetailActionType,
    TransactionDetailDispatch
} from '@/redux/actions/screens/Details/TransactionDetailActionType'
import {
    CategoryListActionType,
    CategoryListDispatch,
} from "./GategoryActionTypes"
import { UserCategory } from '@/data'
import { StateConsumer } from "@/redux/base/ReduxBase"
import Api from '@/api/ApiClient'
import { Strings } from '@/values/Strings'

export const fetchUserCategoryList = (retryCount?: number) => (dispatch: CategoryListDispatch) => {
    dispatch({
        type: CategoryListActionType.GET_USER_CATEGORY_LIST_START
    })
    Api.client.fetchUserCategories()
        .then((response: UserCategory[]) => {
            dispatch({
                type: CategoryListActionType.GET_USER_CATEGORY_LIST_SUCCESS,
                payload: response
            })
        })
        .catch(() => {
            if (!retryCount || retryCount < Api.MAX_RETRIES)
                dispatch(fetchUserCategoryList((retryCount ?? 0) + 1))
            else
                dispatch({
                    type: CategoryListActionType.GET_USER_CATEGORY_LIST_FAIL
                })
        })
}

export const updateTransactionUserCategory = (transactionId: string, userCategory: UserCategory, retryCount?: number) => (dispatch: TransactionDetailDispatch, getState: StateConsumer) => {
    if ((!retryCount && getState().transactionDetailReducers?.isUpdating) ||
        (getState().transactionDetailReducers.transaction?.id !== transactionId)) {
        return
    }
    dispatch({
        type: TransactionDetailActionType.UPDATE_TRANSACTION_USER_CATEGORY_START,
    })
    Api.client.updateTransactionUserCategory(transactionId, userCategory.id)
        .then(() => {
            dispatch({
                type: TransactionDetailActionType.UPDATE_TRANSACTION_USER_CATEGORY_SUCCESS,
                payload: {
                    updatedCategory: userCategory,
                    updatedTransactionId: transactionId
                }
            })
        })
        .catch(() => {
            if (!retryCount || retryCount < Api.MAX_RETRIES) {
                dispatch(updateTransactionUserCategory(transactionId, userCategory, (retryCount ?? 0) + 1))
            } else {
                Toast.show(Strings.CategoryUpdateFailed)
                dispatch({
                    type: TransactionDetailActionType.UPDATE_TRANSACTION_USER_CATEGORY_FAIL
                })
            }
        })
}