import { UserCategory } from "@/data"
import { ActionBase, Dispatch } from '@/redux/base/ReduxBase'
import { Action } from 'redux'

export enum CategoryListActionType {
    GET_USER_CATEGORY_LIST_START = "GET_USER_CATEGORY_LIST_START_ACTION",
    GET_USER_CATEGORY_LIST_SUCCESS = "GET_USER_CATEGORY_LIST_SUCCESS_ACTION",
    GET_USER_CATEGORY_LIST_FAIL = "GET_USER_CATEGORY_LIST_FAIL_ACTION",
}

export enum CategorySelectActionType {
    SELECT_CATEGORY = "SELECT_CATEGORY_ACTION",
    RESET = "SELECT_CATEGORY_RESET_ACTION"
}

export type CategorySelectAction = ActionBase<CategorySelectActionType, string, Action<CategorySelectActionType>>

export interface CategoryUpdatePayload {
    transactionId?: string,
    categoryId?: string,
    categoryName?: string
}

export type CategoryListAction = ActionBase<CategoryListActionType, UserCategory[], Action<CategoryListActionType>>
export interface CategoryListDispatch extends Dispatch<CategoryListActionType, CategoryListAction> { }