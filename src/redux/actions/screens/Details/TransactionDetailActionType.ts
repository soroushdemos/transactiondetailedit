import { Dispatch, ActionBase } from "@/redux/base/ReduxBase"
import { Action } from 'redux'
import { Transaction, UserCategory } from '@/data'

export enum TransactionDetailActionType {
    GET_TRANSACTION_DETAILS_START = "GET_TRANSACTION_DETAILS_START_ACTION",
    GET_TRANSACTION_DETAILS_SUCCESS = "GET_TRANSACTION_DETAILS_SUCCESS_ACTION",
    GET_TRANSACTION_DETAILS_FAIL = "GET_TRANSACTION_DETAILS_FAIL_ACTION",

    INVALIDATE = "TRANSACTION_DETAIL_INVALIDATE",

    UPDATE_TRANSACTION_USER_CATEGORY_START = "UPDATE_TRANSACTION_USER_CATEGORY_START_ACTION",
    UPDATE_TRANSACTION_USER_CATEGORY_SUCCESS = "UPDATE_TRANSACTION_USER_CATEGORY_SUCCESS_ACTION",
    UPDATE_TRANSACTION_USER_CATEGORY_FAIL = "UPDATE_TRANSACTION_USER_CATEGORY_FAIL_ACTION",
    
    SHOW_DIALOG = "SHOW_DIALOG_ACTION",
    HIDE_DIALOG = "HIDE_DIALOG_ACTION",
}

export interface TransactionDetailActionPayload {
    transaction?: Transaction,
    updatedTransactionId?: string,
    updatedCategory?: UserCategory
}

export type TransactionDetailAction = ActionBase<TransactionDetailActionType, TransactionDetailActionPayload, Action<TransactionDetailActionType>>
export type TransactionDetailUpdateCategoryAction = ActionBase<TransactionDetailActionType, string, Action<TransactionDetailActionType>>
export interface TransactionDetailDispatch extends Dispatch<TransactionDetailActionType, TransactionDetailAction> {}
