import {
    TransactionDetailAction,
    TransactionDetailActionType,
    TransactionDetailDispatch
} from './TransactionDetailActionType'
import Api from '@/api/ApiClient'


export const fetchTransaction = (id: string, retryCount?: number) => (dispatch: TransactionDetailDispatch) => {
    dispatch({
        type: TransactionDetailActionType.GET_TRANSACTION_DETAILS_START
    })
    Api.client.fetchTransaction(id)
        .then((response) => {
            dispatch({
                type: TransactionDetailActionType.GET_TRANSACTION_DETAILS_SUCCESS,
                payload: {
                    transaction: response
                }
            })
        })
        .catch(() => {
            if (!retryCount || retryCount < Api.MAX_RETRIES)
                dispatch(fetchTransaction(id, (retryCount ?? 0) + 1))
            else
                dispatch({
                    type: TransactionDetailActionType.GET_TRANSACTION_DETAILS_FAIL
                })
            return false
        })
}

export const invalidateTransaction = () => (dispatch: TransactionDetailDispatch) => {
    dispatch({
        type: TransactionDetailActionType.INVALIDATE
    })
}