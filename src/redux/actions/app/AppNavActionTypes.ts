import { ScreenKey } from './../../../components/screens/ScreenKey'
import { Dispatch, ActionBase } from "@/redux/base/ReduxBase"
import { Action } from "redux"

export enum AppNavActionType {
}

export type AppNavAction = ActionBase<AppNavActionType, ScreenKey, Action<AppNavActionType>>
export interface AppNavDispatch extends Dispatch<AppNavActionType, AppNavAction> {}