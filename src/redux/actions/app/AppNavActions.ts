import { TransactionDetailDispatch, TransactionDetailActionType } from '@/redux/actions/screens/Details/TransactionDetailActionType';
import { StateConsumer } from '@/redux/base/ReduxBase'
import { Actions } from 'react-native-router-flux';

export const navigateBack = (forced?: boolean) => (dispatch: TransactionDetailDispatch, getState: StateConsumer) => {
    const state = getState()
    if (state.transactionDetailReducers.isUpdating && !forced) {
        dispatch({ type: TransactionDetailActionType.SHOW_DIALOG })
        return
    }

    if (state.transactionDetailReducers.dialogVisible) return

    Actions.pop()
}