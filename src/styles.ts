import { StyleSheet } from "react-native"
import { Colors } from "@/values/Colors"

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.White,
  },
})
