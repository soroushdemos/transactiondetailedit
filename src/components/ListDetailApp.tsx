import React from 'react'
import { StyleSheet } from 'react-native'
import TransactionList from './screens/TransactionList/TransactionListScreenComponent'
import TransactionDetail from './screens/TransactionDetaill/TransactionDetailScreenComponent'
import SelectCategory from './screens/ChangeCategory/ChangeCategoryScreenComponent'
import { Router, Scene, Stack } from 'react-native-router-flux'
import { Colors } from '@/values/Colors'
import { Strings } from '@/values/Strings'
import NavBarComponent from './common/Navbar/NavbarComponent'
import { ScreenKey } from './screens/ScreenKey'
import { connect } from 'react-redux'
import { AppNavDispatch } from '@/redux/actions/app/AppNavActionTypes'
import { navigateBack } from '@/redux/actions/app/AppNavActions'
import { AppReducersState } from '@/redux/store'


class ListDetailsApp extends React.Component<AppProps> {
    /// React.Component Methods
    render() {
        return (
            <Router
                sceneStyle={styles.container}
                backAndroidHandler={this.props.navigateBack}>
                <Stack key="root">
                    <Scene
                        title={Strings.Transactions}
                        component={TransactionList}
                        navBar={NavBarComponent}
                        key={ScreenKey.TransactionList}
                    />
                    <Scene
                        title={Strings.Details}
                        headerBackTitle={Strings.Transactions}
                        navBar={NavBarComponent}
                        component={TransactionDetail}
                        key={ScreenKey.TransactionDetail}
                        gesturesEnabled={false}
                    />
                    <Scene
                        title={Strings.ChangeCategory}
                        headerBackTitle={Strings.Details}
                        component={SelectCategory}
                        navBar={NavBarComponent}
                        key={ScreenKey.SelectCategory}
                    />
                </Stack>
            </Router>
        )
    }
}

/**********
 * Helpers
 **********/
const mapStateToProps = (state: AppReducersState) => ({})
const mapDispatchToProps = (dispatch: AppNavDispatch): AppDispatchProps => ({
    navigateBack: () => {
        dispatch(navigateBack())
        return true
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(ListDetailsApp)


/**************
 * Prop Types
 **************/
interface AppDispatchProps {
    navigateBack: () => boolean
}

interface AppProps extends AppDispatchProps { }

/**********
 * Styles *
 **********/
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.Background,
    },
    pageTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "flex-end"
    }
})
