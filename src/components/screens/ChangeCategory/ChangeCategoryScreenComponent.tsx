import React from 'react'
import { FlatList, View } from 'react-native'
import { connect } from 'react-redux'
import { fetchUserCategoryList, updateTransactionUserCategory } from '@/redux/actions/screens/Category/GategoryActions'
import { UserCategory } from '@/data'
import UserCategoryListItem from '@/components/common/ListItem/UserCategory/UserCategoryListItemComponent'
import { Dispatch } from '@/redux/base/ReduxBase'
import LoadingView from '@/components/common/ScreenLoading/ScreenLoadingComponent'
import { AppReducersState } from '@/redux/store'
import { CategoryReducerState } from '@/redux/reducers/screens/CategoryReducers'
import { CategorySelectActionType } from '@/redux/actions/screens/Category/GategoryActionTypes'
import RetryView from '@/components/common/RetryView'


class ChangeCategoryScreenComponent extends React.Component<ChangeCategoryScreenProps> {
    /// React Componenet Methods
    componentWillMount() {
        this.props.fetchUserCategoryList()
    }

    componentWillUnmount() {
        if (this.props.newSelectedId &&
            this.props.selectedId !== this.props.newSelectedId) {
            const newCategory = this.props.categories?.find(({ id }) => id === this.props.newSelectedId)
            if (newCategory)
                this.props.updateTransactionUserCategory(this.props.transactionId, newCategory)
        }
        this.props.reset()
    }

    render() {
        if (this.props.isLoading) return (<LoadingView />)
        if (!this.props.categories) return (
            <RetryView callback={this.props.fetchUserCategoryList} />
        )
        return (
            <View>
                <FlatList
                    extraData={[this.props.selectedId]}
                    data={this.props.categories}
                    renderItem={({ item }) => { return this.renderRow(item) }}
                    keyExtractor={item => item.id}
                />
            </View>
        )
    }

    /// Helper Methods
    private renderRow(item: UserCategory) {
        return (
            <UserCategoryListItem
                id={item.id}
                name={item.name}
                isChecked={this.isSelected(item)}
                onCategoryTapped={() => {
                    this.props.selectCategory(item.id)
                }}
            />
        )
    }

    private isSelected(item: UserCategory): boolean {
        return ((this.props.newSelectedId && this.props.newSelectedId === item.id) ||
            (!this.props.newSelectedId && this.props.selectedId === item.id))
    }
}

const mapStateToProps = (state: AppReducersState): ChangeCategoryScreenComponentStateProps =>
    Object.assign({},
        state.categoryListReducer,
        state.categorySelectReducer,
    )

const mapDispatchToProps = (dispatch: Dispatch<any, any>): ChangeCategoryScreenComponentDispatchProps => ({
    fetchUserCategoryList: () => { dispatch(fetchUserCategoryList()) },
    selectCategory: (categoryId) => {
        dispatch({
            type: CategorySelectActionType.SELECT_CATEGORY,
            payload: categoryId
        })
    },
    updateTransactionUserCategory: (transactionId, category) => {
        dispatch(updateTransactionUserCategory(transactionId, category))
    },
    reset: () => dispatch({
        type: CategorySelectActionType.RESET
    })
})


export default connect(mapStateToProps, mapDispatchToProps)(ChangeCategoryScreenComponent)

/*************
 * Prop Types
 *************/

interface ChangeCategoryScreenComponentProps {
    selectedId: string,
    transactionId: string
}

interface ChangeCategoryScreenComponentStateProps extends CategoryReducerState { }
interface ChangeCategoryScreenComponentDispatchProps {
    fetchUserCategoryList: () => void
    selectCategory: (categoryId: string) => void
    updateTransactionUserCategory: (transactionId: string, category: UserCategory) => void
    reset: () => void
}

interface ChangeCategoryScreenProps extends
    ChangeCategoryScreenComponentProps, ChangeCategoryScreenComponentStateProps, ChangeCategoryScreenComponentDispatchProps { }