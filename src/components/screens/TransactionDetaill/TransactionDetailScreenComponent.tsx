import React from "react"
import { View, Text, TouchableOpacity, Linking, ActivityIndicator } from "react-native"
import { connect } from "react-redux"
import { fetchTransaction, invalidateTransaction } from "@/redux/actions/screens/Details/TransactionDetailActions"
import styles from "./TransactionDetailScreenStyle"
import { formatPrice } from '@/helper/NumberFormatter'
import { formatDate } from '@/helper/DateFormatter'
import { Actions as Navigate, Actions } from "react-native-router-flux"
import { TransactionDetailDispatch, TransactionDetailActionType } from "@/redux/actions/screens/Details/TransactionDetailActionType"
import LoadingView from '@/components/common/ScreenLoading/ScreenLoadingComponent'
import { Strings } from "@/values/Strings"
import RightArrow from '@/icons/arrow-right.svg'
import { Colors } from "@/values/Colors"
import { ScreenKey } from "../ScreenKey"
import { AppReducersState } from "@/redux/store"
import { TransactionDetailReducerState } from "@/redux/reducers/screens/TransactionDetailReducers"
import RetryView from "@/components/common/RetryView"
import { Transaction, UserCategory } from "@/data"
import DialogComponent from "@/components/common/Dialog/DialogComponent"
import { navigateBack } from "@/redux/actions/app/AppNavActions"
import Toast from 'react-native-tiny-toast'

class DetailScreenComponent extends React.Component<DetailScreenProps> {
    /// React.Component Methods
    componentWillMount() {
        this.props.fetchTransaction(this.props.transactionId)
    }

    componentWillUnmount() {
        this.props.invalidateTransaction()
    }

    render() {
        if (this.props.isLoading) return (<LoadingView />)
        if (!this.props.transaction)
            return (<RetryView callback={() =>
                this.props.fetchTransaction(this.props.transactionId)} />)
        return this.renderTable(this.props.transaction)
    }

    /// Helper Methods
    private renderTable(transaction: Transaction) {
        return (
            <View style={styles.container}>
                <View style={styles.firstSection}>
                    <View style={styles.leftColumn}>
                        <Text style={styles.mainTitle}>{transaction.merchant.name}</Text>
                        <Text style={styles.subTitle}>{transaction.merchant.merchantCategory.name}</Text>
                    </View>
                    <Text style={styles.rightProperty}>{formatPrice(transaction.amount)}</Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.sectionTitle}>{Strings.Details}</Text>
                    <View>
                        <View style={styles.detailRow}>
                            <Text style={styles.subTitle}>{Strings.PurchasedAt}</Text>
                            <Text style={styles.info}>{formatDate(transaction.purchaseTime)}</Text>
                        </View>
                        <View style={styles.detailRow}>
                            <Text style={styles.subTitle}>{Strings.MerchantName}</Text>
                            <Text style={styles.info}>{transaction.merchant.name}</Text>
                        </View>
                        <View style={styles.detailRow}>
                            <Text style={styles.subTitle}>{Strings.Website}</Text>
                            <TouchableOpacity
                                onPress={() => this.openUrl(transaction.merchant.website)}>
                                <Text style={styles.highLightedInfo}>{transaction.merchant.website}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <TouchableOpacity
                    onPress={() =>
                        this.navigateToCategorySelect(this.getTransactionUserCategory()?.id)}>
                    <View style={styles.section}>
                        <Text style={styles.sectionTitle}>{Strings.Accounting}</Text>
                        <View style={styles.detailRow}>
                            <Text style={styles.subTitle}>{Strings.QuickBooksCategory}</Text>
                            {this.renderCategorySection()}
                        </View>
                    </View>
                </TouchableOpacity>
                {this.renderDialog()}
            </View>
        )
    }

    private openUrl(url: string) {
        if (!url.includes("http://") && !url.includes("https://"))
            url = `http://${url}`

        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url)
            } else {
                Toast.show(Strings.InvalidUrl.replace("%s", url))
            }
        })
    }

    private getTransactionUserCategory(): UserCategory | undefined {
        return this.props.updatedCategory ?? this.props.transaction?.integration.category ?? undefined
    }

    private renderCategorySection() {
        if (this.props.isUpdating) return (
            <ActivityIndicator color={Colors.HighLight} size="small" />
        )
        else return (
            <View style={{ flexDirection: 'row' }}>
                <Text style={styles.highLightedInfo}>
                    {this.getTransactionUserCategory()?.name}
                </Text>
                <RightArrow style={styles.arrow} />
            </View>
        )
    }

    private navigateToCategorySelect(selectedCategoryId?: string) {
        if (!this.props.isUpdating && Actions.currentScene == ScreenKey.TransactionDetail)
            Navigate[ScreenKey.SelectCategory]({
                transactionId: this.props.transactionId,
                selectedId: selectedCategoryId
            })
    }

    private renderDialog() {
        if (this.props.dialogVisible)
            return (
                <DialogComponent
                    isVisible={this.props.dialogVisible}
                    title={Strings.UnsavedChangesDialogTitle}
                    description={Strings.UnsavedChangesDialogDescription}
                    positiveTitle={Strings.Yes}
                    positivieCallback={() => {
                        this.props.hideDialog()
                        this.props.navigateBack(true)
                    }}
                    negativeTitle={Strings.No}
                    negativeCallback={() => {
                        this.props.hideDialog()
                    }}
                />
            )
    }
}

const mapStateToProps = (state: AppReducersState): DetailScreenStateProps =>
    state.transactionDetailReducers

const mapDispatchToProps = (dispatch: TransactionDetailDispatch): DetailScreenDispatchProps => ({
    fetchTransaction: (id: string) => { dispatch(fetchTransaction(id)) },
    invalidateTransaction: () => { dispatch(invalidateTransaction()) },
    showDialog: () => dispatch({ type: TransactionDetailActionType.SHOW_DIALOG }),
    hideDialog: () => dispatch({ type: TransactionDetailActionType.HIDE_DIALOG }),
    navigateBack: (forced) => dispatch(navigateBack(forced))
})

export default connect(mapStateToProps, mapDispatchToProps)(DetailScreenComponent)

/**************
 * Prop Types
 **************/

interface DetailScreenComponentProps {
    transactionId: string
}

interface DetailScreenStateProps extends TransactionDetailReducerState { }

interface DetailScreenDispatchProps {
    fetchTransaction: (id: string) => void,
    invalidateTransaction: () => void,
    showDialog: () => void,
    hideDialog: () => void,
    navigateBack: (forced: boolean) => void
}

interface DetailScreenProps extends DetailScreenComponentProps,
    DetailScreenStateProps, DetailScreenDispatchProps { }