import { StyleSheet, ViewStyle } from "react-native"
import { Colors } from "@/values/Colors"

const section: ViewStyle = {
  flexDirection: 'column',
  backgroundColor: Colors.White,
  marginBottom: 4,
  paddingLeft: 14,
  paddingRight: 14,
  paddingTop: 16,
  paddingBottom: 16
}

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "stretch"
  },
  section: section,
  firstSection: {
    ...section,
    flexDirection: 'row',
  },
  sectionTitle: {
    textTransform: 'uppercase',
    marginBottom: 12,
    fontWeight: "500"
  },
  sectionBody: {
    flexDirection: "row"
  },
  detailRow: {
    flexDirection: 'row',
    alignItems: "stretch",
    justifyContent: "space-between",
    lineHeight: 17,
    marginBottom: 8
  },
  leftColumn: {
    flexDirection: "column",
    alignItems: "stretch",
    flex: 1
  },
  mainTitle: {
    fontSize: 16,
    fontWeight: "500",
    marginBottom: 8,
    color: Colors.PrimaryText
  },
  info: {
    fontSize: 12,
    color: Colors.PrimaryText,
    fontWeight: "500",
  },
  highLightedInfo: {
    fontSize: 12,
    color: Colors.HighLight,
    fontWeight: "500",
    alignSelf: "center"
  },
  arrow: {
    color: Colors.HighLight,
    width: 4,
    height: 7
  },
  subTitle: {
    fontSize: 12,
    color: Colors.SecondaryText,
    fontWeight: "500"
  },
  rightProperty: {
    fontSize: 16,
    fontWeight: "bold",
    alignSelf: "center"
  }
})
