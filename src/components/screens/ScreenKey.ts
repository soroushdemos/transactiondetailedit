export enum ScreenKey {
    TransactionList = "TRANSACTION_LIST_SCREEN",
    TransactionDetail = "TRANSACTION_DETAIL_SCREEN",
    SelectCategory = "UPDATE_TRANSACTION_USER_CATEGORY_SCREEN"
}
