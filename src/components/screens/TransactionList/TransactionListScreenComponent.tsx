import React from "react"
import { View, FlatList, Text } from "react-native"
import styles from "./TransactionListScreenStyle"
import ListItemComponent from "@/components/common/ListItem/Transaction/TransactionListItemComponent"
import { fetchTransactions } from '@/redux/actions/screens/Transactions/TransactionActions'
import { connect } from 'react-redux'
import { Transaction } from "@/data"
import { TransactionDispatch } from "@/redux/actions/screens/Transactions/TransactionActionType"
import LoadingView from '@/components/common/ScreenLoading/ScreenLoadingComponent'
import RetryView from "@/components/common/RetryView"
import { TransactionReducerState } from "@/redux/reducers/screens/TransactionReducers"
import { AppReducersState } from "@/redux/store"
import { Strings } from "@/values/Strings"

class ListScreenComponent extends React.Component<ListScreenProps> {
    /// React.Component Methods
    componentWillMount() {
        this.props.fetchTransactions()
    }

    render() {
        if (this.props.isLoading) return (<LoadingView />)
        if (this.props.transactions == undefined)
            return (<RetryView callback={this.props.fetchTransactions} />)
        if (this.props.transactions.length == 0)
            return this.emptyView()

        return (
            <View style={styles.container}>
                <FlatList
                    data={this.props.transactions}
                    renderItem={({ item }) => this.renderListItem(item)}
                    keyExtractor={item => item.id}
                />
            </View>
        )
    }

    /// Helper Methods
    private renderListItem(item: Transaction) {
        return (<ListItemComponent
            id={item.id}
            price={item.amount}
            title={item.merchant.name}
            category={item.merchant.merchantCategory.name}
        />)
    }

    private emptyView() {
        return (
            <View style={styles.emptyContainer}>
                <Text style={styles.emptyText}>{Strings.NothingToShow}</Text>
            </View>
        )
    }
}

const mapStateToProps = (state: AppReducersState): ListScreenStateProps =>
    state.transactionReducers


const mapDispatchToProps = (dispatch: TransactionDispatch): ListScreenDispatchProps => ({
    fetchTransactions: () => { dispatch(fetchTransactions()) }
})

export default connect(mapStateToProps, mapDispatchToProps)(ListScreenComponent)

/*************
 * Prop Types
 *************/

interface ListScreenStateProps extends TransactionReducerState { }

interface ListScreenDispatchProps {
    fetchTransactions: () => void
}

interface ListScreenProps extends TransactionReducerState, ListScreenDispatchProps { }