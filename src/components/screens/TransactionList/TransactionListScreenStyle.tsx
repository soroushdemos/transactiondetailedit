import { StyleSheet } from "react-native"
import { Colors } from "@/values/Colors"

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  emptyContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
  },
  emptyText: {
    fontSize: 16,
    flex: 1,
    textAlign: "center"
  }
})
