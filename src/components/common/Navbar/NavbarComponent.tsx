import React from 'react'
import { View, Text, TouchableOpacity, StatusBar } from 'react-native'
import styles from './NavBarStyles'
import LeftArrow from '@/icons/arrow-left.svg'
import { Colors } from '@/values/Colors'
import { connect } from 'react-redux'
import { navigateBack } from '@/redux/actions/app/AppNavActions'
import { AppNavDispatch } from '@/redux/actions/app/AppNavActionTypes'


class NavBarComponent extends React.Component<NavBarProps> {
    /// React.Component Methods
    render() {
        return (
            <View style={styles.container}>
                <StatusBar translucent={true} backgroundColor={Colors.White} barStyle="dark-content" />
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>{this.props.title}</Text>
                </View>
                {this.renderBackSection(this.props.headerBackTitle)}
                {/* {this.renderDialog()} */}
            </View>
        )
    }

    /// Helper Methods
    private renderBackSection(title: string) {
        if (title && title !== "")
            return (
                <TouchableOpacity
                    style={styles.touchable}
                    onPress={() => {
                        this.props.navigateBack()
                    }}>
                    <View style={styles.backSection}>
                        <LeftArrow style={styles.arrow} />
                        <Text style={styles.headerBackTitle}>{title}</Text>
                    </View>
                </TouchableOpacity>
            )
    }
}

const mapStateToProps = () => ({})
const mapDispatchToProps = (dispatch: AppNavDispatch): NavBarDispatchProps => ({
    navigateBack: () => dispatch(navigateBack()),
})

export default connect(mapStateToProps, mapDispatchToProps)(NavBarComponent)

/**************
 * Prop Typess
 **************/
interface NavBarComponentProps {
    title: string,
    headerBackTitle: string
}


interface NavBarDispatchProps {
    navigateBack(forced?: boolean): void,
}


interface NavBarProps extends NavBarComponentProps, NavBarDispatchProps { }