import { StyleSheet } from "react-native"
import { Colors } from "@/values/Colors"

const styles = StyleSheet.create({
    container: {
        marginBottom: 8,
        backgroundColor: Colors.White,
        height: 88,
        flexDirection: "row",
        alignItems: "center",
        paddingBottom: 5,
    },
    touchable: {
        alignSelf: "flex-end"
    },
    backSection: {
        flexDirection: "row",
        alignSelf: "flex-end"
    },
    arrow: {
        paddingLeft: 40,
         color: Colors.HighLight 
    },
    headerBackTitle: {
        color: Colors.HighLight,
        fontSize: 16
    },
    titleContainer: {
        alignItems: "center",
        flex: 1,
        flexDirection: "row",
        position: "absolute",
        left: 0, right: 0, bottom: 5
    },
    title: {
        lineHeight: 26,
        flex: 1,
        textAlign: "center",
        color: Colors.PrimaryText,
        fontWeight: "500",
        fontSize: 16,
    }
})

export default styles