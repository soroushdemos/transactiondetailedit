import { StyleSheet } from "react-native"
import { Colors } from "@/values/Colors"

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        paddingTop: 16,
        paddingBottom: 16,
        paddingLeft: 14,
        paddingRight: 14,
        marginBottom: 1,
        backgroundColor: Colors.White,
        alignItems: "center"
    },
    title: {
        flex: 1,
        fontSize: 14
    },
    checkMark: {
        color: Colors.HighLight,
    },
    progress: {
        color: Colors.HighLight
    }
})