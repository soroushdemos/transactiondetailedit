import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './UserCategoryListItemStyles'
import Check from '@/icons/check.svg'

class UserCategoryListItem extends React.Component<UserCategoryListItemProps> {
    /// Reac.Component Methods
    render() {
        return (
            <TouchableOpacity
                onPress={() => this.props.onCategoryTapped()}>
                <View style={styles.container}>
                    <Text style={styles.title}>{this.props.name}</Text>
                    {this.renderCheckMark()}
                </View>
            </TouchableOpacity>
        )
    }

    /// Helper Methods
    private renderCheckMark() {
        if (this.props.isChecked)
            return (
                <Check style={styles.checkMark} />
            )
    }
}

export default UserCategoryListItem

/*************
 * Prop Types
 *************/
interface UserCategoryListItemProps {
    id: string,
    name: string,
    isChecked: boolean,
    onCategoryTapped: () => void
}