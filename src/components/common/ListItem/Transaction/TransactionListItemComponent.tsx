import React from "react"
import { View, Text, TouchableOpacity } from "react-native"
import styles from "./TransactionListItemStyle"
import { Actions as Navigate } from "react-native-router-flux"
import { formatPrice } from "@/helper/NumberFormatter"
import { ScreenKey } from "@/components/screens/ScreenKey"


const navigateToDetail = (transactionId: string) => {
    if (Navigate.currentScene == ScreenKey.TransactionList)
        Navigate[ScreenKey.TransactionDetail]({
            transactionId: transactionId
        })
}

const ListItemComponent = ({ id, title, category, price }: ListItemComponentProps) => (
    <TouchableOpacity
        onPress={() => navigateToDetail(id)}>
        <View key={id} style={styles.container}>
            <View style={styles.leftColumn}>
                <Text style={styles.mainTitle}>{title}</Text>
                <Text style={styles.subTitle}>{category}</Text>
            </View>
            <Text style={styles.rightProperty}>{formatPrice(price)}</Text>
        </View>
    </TouchableOpacity >
)

export default ListItemComponent

/*************
 * Prop Types
 *************/
export interface ListItemComponentProps {
    id: string,
    title: string,
    category: string,
    price: number
}