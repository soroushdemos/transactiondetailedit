import { StyleSheet } from "react-native"
import { Colors } from "@/values/Colors"

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Colors.White,
    paddingTop: 16,
    paddingBottom: 16,
    paddingLeft: 14,
    paddingRight: 14,
    marginBottom: 2
  },
  leftColumn: {
    flex: 1
  },
  mainTitle: {
    fontSize: 16,
    fontWeight: "500",
    marginBottom: 8,
    color: Colors.PrimaryText
  },
  subTitle: {
    fontSize: 12,
    color: Colors.SecondaryText,
    fontWeight: "500"
  },
  rightProperty: {
    fontSize: 16,
    fontWeight: "bold",
    alignSelf: "center"
  }
})
