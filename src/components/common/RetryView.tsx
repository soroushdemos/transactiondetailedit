import React from 'react'

import { View, TouchableOpacity, StyleSheet, Text } from "react-native"
import { Strings } from '@/values/Strings'
import { Colors } from "@/values/Colors"

export default ({ callback }: RetryProps) => (
    <View style={styles.retryViewContainer}>
        <View style={{ flexDirection: "column" }}>
            <Text style={styles.retryText}>{Strings.LoadingFailed}</Text>
            <TouchableOpacity
                onPress={callback}>
                <Text style={styles.retryButton}>{Strings.Retry}</Text>
            </TouchableOpacity>
        </View>
    </View>
)

/***************
 * Prop Types
 ***************/
interface RetryProps {
    callback: () => void
}

/***************
 * Styles
 ***************/
const styles = StyleSheet.create({
    retryViewContainer: {
        backgroundColor: Colors.White,
        flex: 1,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    retryText: {
        alignSelf: "center",
        fontSize: 18,
        paddingBottom: 20
    },
    retryButton: {
        alignSelf: "center",
        fontSize: 16,
        color: Colors.HighLight
    }
})