import React, { Component } from "react"
import Dialog from "react-native-dialog"
import styles from "./DialogComponentStyles"

export default (props: DialogComponentProps) => (
    <Dialog.Container visible={props.isVisible}>
        <Dialog.Title style={styles.title}>{props.title}</Dialog.Title>
        <Dialog.Description style={styles.description}>
            {props.description}
        </Dialog.Description>
        <Dialog.Button
            style={styles.negativeButton}
            label={props.negativeTitle}
            onPress={props.negativeCallback} />
        <Dialog.Button
            style={styles.positiveButton}
            label={props.positiveTitle}
            onPress={props.positivieCallback} />
    </Dialog.Container>
)

/**************
 * Prop Types
 **************/
export type DialogComponentProps = {
    isVisible: boolean
    title: string
    description: string
    positiveTitle: string
    positivieCallback: () => void
    negativeTitle: string
    negativeCallback: () => void
}