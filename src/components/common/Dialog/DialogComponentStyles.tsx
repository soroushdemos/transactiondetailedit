import { StyleSheet } from "react-native"
import { Colors } from "@/values/Colors"

export default StyleSheet.create({
    title: {
        textAlign: "center",
        marginBottom: 16,
        color: Colors.PrimaryText
    },
    description: {
        textAlign: "center",
        fontSize: 14,
        color: Colors.SecondaryText
    },
    negativeButton: {
        color: Colors.HighLight
    },
    positiveButton: {
        marginLeft: 20,
        color: Colors.HighLight
    }
})