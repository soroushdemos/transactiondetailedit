import React from 'react'

import { View, ActivityIndicator } from "react-native"
import styles from './ScreenLoadingStyles'
import { Colors } from '@/values/Colors'

const ScreenLoadingComponent = () => (
    <View style={styles.constainer}>
        <ActivityIndicator size="large" color={Colors.HighLight} />
    </View>
)

export default ScreenLoadingComponent