import Client from '@/data'

export default class Api {
    static MAX_RETRIES = 3
    static client = new Client()
}
