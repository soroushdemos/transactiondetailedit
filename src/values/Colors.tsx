export enum Colors {
    White = "#ffffff",
    PrimaryText = "#000000",
    SecondaryText = "#9FA2A6",
    HighLight = "#E9794B",
    Background = "#FAFAFA"
}