export enum Strings {
    Transactions = "Transactions",
    Details = "Details",
    ChangeCategory = "Change Category",

    PurchasedAt = "Purchased at",
    MerchantName = "Merchant name",
    Website = "Website",
    Accounting = "Accounting",
    QuickBooksCategory = "Quickbooks Category",

    InvalidUrl="Oops! '%s' seems to be invalid",

    UnsavedChangesDialogTitle = "Changes may be lost!",
    UnsavedChangesDialogDescription = "Updating still in progress, are you sure?",
    Yes = "Yes",
    No = "No",

    LoadingFailed = "Loading Failed!",
    Retry = "Retry",
    NothingToShow = "Nothing to show!",

    CategoryUpdateFailed = "Category update failed! Please try agin."
}