export const formatPrice = (n: number): string => {
    const currencySymbol = "$"
    const change = `${n % 100}`
    const bills = Math.floor(n / 100)

    return `${currencySymbol}${bills}.${change}${change.length == 1 ? "0" : ""}`
}