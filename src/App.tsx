import React from "react"

import { View } from "react-native"
import styles from "./styles"
import ListDetailsApp from "./components/ListDetailApp"
import { Provider } from "react-redux"
import store from "./redux/store"

const App = () => (
  <View testID="app-container" style={styles.container}>
    <Provider store={store}>
      <ListDetailsApp />
    </Provider>
  </View>
)

export default App
